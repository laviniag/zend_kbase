<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 17:15
 */
namespace Article;

use Zend\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            Controller\ArticleController::class => Factory\ArticleFactory::class,
        ]
    ],
    'form_elements' => [
        'factories' => [
            Form\ArticleForm::class => Factory\ArticleFormFactory::class,
        ]
    ],
    'router' => [
        'routes' => [
            'article' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/article[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ArticleController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'article' => __DIR__ . '/../view',
        ],
    ],
];