<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 17:29
 */
namespace Article\Controller;

use Article\Model\Article;
use Article\Form\ArticleForm;
use Article\Form\ArticleFactory;
use Article\Model\ArticleTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ArticleController extends AbstractActionController
{
    private $table;
    private $cat_options;
    private $session;

    public function __construct(ArticleTable $table, $session, array $cat_options = [])
    {
        $this->table = $table;
        $this->cat_options = $cat_options;
        $this->session = $session;
    }

    public function homeAction()
    {
        $this->session->prev_page = 'home';
        return new ViewModel([
            'articles' => $this->table->fetchAll(),
            'categories' => $this->cat_options
        ]);
    }

    public function indexAction()
    {
        $this->session->prev_page = 'index';
        return new ViewModel([
            'articles' => $this->table->fetchAll(),
        ]);
    }

    public function listAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (0 === $id) {
            return $this->redirect()->toRoute('article', ['action' => 'home']);
        }

        $this->session->prev_page = 'list/'.$id;
        return new ViewModel([
            'articles' => $this->table->fetchByCategory($id),
            'category' => $this->cat_options[$id],
        ]);
    }

    public function addAction()
    {
        $form = new ArticleForm( $this->cat_options );
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $article = new Article();
        $form->setInputFilter($article->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $article->exchangeArray($form->getData());
        $this->table->saveArticle($article);
        return $this->redirect()->toRoute('article');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('article', ['action' => 'add']);
        }

        // Retrieve the article with the specified id.
        try {
            $article = $this->table->getArticle($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('article', ['action' => 'index']);
        }

        $form = new ArticleForm( $this->cat_options );
        $form->bind($article);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($article->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->savearticle($article);

        // Redirect to article list
        return $this->redirect()->toRoute('article', ['action' => 'index']);
    }

    public function viewAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('article', ['action' => 'home']);
        }

        // Retrieve the article with the specified id.
        try {
            $article = $this->table->getArticle($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('article', ['action' => 'home']);
        }

        $page = explode('/',$this->session->prev_page);

        $viewData = ['article' => $article,
            'category' => $this->cat_options[$article->category_id],
            'page' => $page,
        ];
        return $viewData;
     }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('article');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteArticle($id);
            }

            // Redirect to list of articles
            return $this->redirect()->toRoute('article');
        }

        return [
            'id'    => $id,
            'article' => $this->table->getArticle($id),
        ];
    }
}