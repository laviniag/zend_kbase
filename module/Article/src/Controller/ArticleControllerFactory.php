<?php

use Article\Controller\ArticleController;
use Article\Form\ArticleForm;
use Interop\Container\ContainerInterface;

class ArticleControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $formManager = $container->get('FormElementManager');

        return new ArticleController(
            $formManager->get(ArticleForm::class)
        );
    }
}