<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 22:31
 */
namespace Article\Model;

use DomainException;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;

class Article
{
    public $id;
    public $category_id;
    public $title;
    public $body;
    public $keywords;
    public $create_date;

    private $inputFilter;

    public function exchangeArray(array $data)
    {
        $this->id     = !empty($data['id']) ? $data['id'] : null;
        $this->category_id = !empty($data['category_id']) ? $data['category_id'] : null;
        $this->title  = !empty($data['title']) ? $data['title'] : null;
        $this->body  = !empty($data['body']) ? $data['body'] : null;
        $this->keywords  = !empty($data['keywords']) ? $data['keywords'] : null;
        $this->create_date  = !empty($data['create_date']) ? $data['create_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'category_id' => $this->category_id,
            'title'  => $this->title,
            'body'  => $this->body,
            'keywords'  => $this->keywords,
            'create_date'  => $this->create_date,
        ];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'category_id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'body',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 500,
                    ],
                ],
            ],
        ]);

        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
}