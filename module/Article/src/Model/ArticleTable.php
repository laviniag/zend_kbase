<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 22:40
 */
namespace Article\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class ArticleTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function fetchByCategory($id)
    {
        return $this->tableGateway->select(['category_id' => $id]);
    }

    public function getArticle($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveArticle(Article $article)
    {
        $data = [
            'category_id' => $article->category_id,
            'title'  => $article->title,
            'body'  => $article->body,
            'keywords'  => $article->keywords
        ];
    
        $id = (int) $article->id;
    
        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }
    
        if (! $this->getArticle($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update Article with identifier %d; does not exist',
                $id
            ));
        }
    
        $this->tableGateway->update($data, ['id' => $id]);
    }
    
    public function deleteArticle($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}