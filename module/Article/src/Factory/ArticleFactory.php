<?php
namespace Article\Factory;

use Article\Model\ArticleTable;
use Category\Model\CategoryTable;
use Article\Controller\ArticleController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ArticleFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return ArticleForm
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $session = $container->get('ArticleSession');
        $articleTable = $container->get(ArticleTable::class);
        $categoryTable = $container->get(CategoryTable::class);
        $categories = $categoryTable->fetchAll();
        foreach ($categories as $category) {
            $options[$category->id] = $category->name;
        }
        asort($options);
        return new ArticleController($articleTable, $session, $options);
    }
}