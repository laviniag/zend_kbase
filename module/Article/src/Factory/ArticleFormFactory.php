<?php
namespace Article\Factory;

use Article\Form\ArticleForm;
use Category\Model\CategoryTable;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

//  This class is not in use for not working
class ArticleFormFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return ArticleForm
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $categoryTable = $container->get(CategoryTable::class);
        $categories = $categoryTable->fetchAll();
        foreach ($categories as $category) {
            $options[$category->id] = $category->name;
        }
        asort($options);
        return new ArticleForm($options);
    }
}