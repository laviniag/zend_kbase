<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 21/08/2017
 * Time: 00:45
 */
namespace Article\Form;

use Zend\Form\Form;

class ArticleForm extends Form
{
    public function __construct(array $cat_options)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('article');
//        print_r($cat_options);
//        die();

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'name' => 'category_id',
            'type' => 'Zend\Form\Element\Select',
            'options' => [
                'label' => 'Select Category',
                'value_options' => $cat_options,
            ],
        ]);

        $this->add([
            'name' => 'body',
            'type' => 'textarea',
            'rows' => 5,
            'options' => [
                'label' => 'Content',
            ],
        ]);

        $this->add([
            'name' => 'keywords',
            'type' => 'text',
            'options' => [
                'label' => 'Keywords',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}