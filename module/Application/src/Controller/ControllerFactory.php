<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 21/08/2017
 * Time: 21:03
 */
namespace Application\Controller;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = (null === $options) ? new $requestedName : new $requestedName($options);

        return $service->setServiceManager($container);
    }
}