<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 17:15
 */
namespace Category;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'category' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/category[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\CategoryController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'category' => __DIR__ . '/../view',
        ],
    ],
];