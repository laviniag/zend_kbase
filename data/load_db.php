<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/08/2017
 * Time: 21:59
 */

$db = new PDO('mysql:host=localhost;dbname=kbase','root','' );

$fh = fopen(__DIR__ . '/schema.sql', 'r');
while ($line = fread($fh, 4096)) {
    try {
        $db->exec($line);
    } catch(PDOException $ex) {
        print_r($ex);
    }
}
fclose($fh);